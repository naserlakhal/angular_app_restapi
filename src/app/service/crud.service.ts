import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, catchError, map, throwError } from 'rxjs';
import { Book } from './book';

@Injectable({
  providedIn: 'root'
})
export class CrudService {
  REST_API: string = 'http://127.0.0.1:8000/api/books';
  HttpHeaders = new HttpHeaders().set('Content-Type', 'application/json');


  constructor(private httpClient: HttpClient) { }
  addBook(data: Book): Observable<any> {
    let API_URL = `${this.REST_API}`;
    return this.httpClient.post(API_URL, data, { headers: this.HttpHeaders }).pipe(catchError(this.handleError));
  }

  getBooks(): Observable<any> {
    let API_URL = `${this.REST_API}`;
    return this.httpClient.get(API_URL, { headers: this.HttpHeaders }).pipe(catchError(this.handleError));
  }
  getBook(id: any): Observable<any> {
    let API_URL = `${this.REST_API}/${id}`;
    return this.httpClient.get(API_URL, { headers: this.HttpHeaders })
      .pipe(map(res => {
        return res || {};
      }),
        catchError(this.handleError));
  }
  updateBook(id: any, data: Book): Observable<any> {
    let API_URL = `${this.REST_API}/${id}`;
    return this.httpClient.put(API_URL, data, { headers: this.HttpHeaders })
      .pipe(catchError(this.handleError));
  }
  deleteBook(id: any): Observable<any> {
    console.log('hello iam in delete services')
    let API_URL = `${this.REST_API}/${id}`;
    return this.httpClient.delete(API_URL,  { headers: this.HttpHeaders })
      .pipe(catchError(this.handleError));
  }
  handleError(error: HttpErrorResponse) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      errorMessage = `An error occured: ${error.error.message}`;
    } else {
      errorMessage = `Server returned code: ${error.status}, error message is: ${error.message}`;
    }
    console.error(errorMessage);
    return throwError(() => new Error(errorMessage));
  }

}
