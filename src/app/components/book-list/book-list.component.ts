import { Book } from './../../service/book';
import { Component, OnInit } from '@angular/core';
//import crudService
import { CrudService } from '../../service/crud.service';
// import book class
@Component({
  selector: 'app-book-list',
  templateUrl: './book-list.component.html',
  styleUrls: ['./book-list.component.css']
})
export class BookListComponent implements OnInit {
  books:any = []

  constructor(
    private crudService: CrudService,
  ) { }

  ngOnInit(): void {
    this.crudService.getBooks().subscribe(data => {
      console.log('checking the list books in ngonInit',data)
      this.books = data as Book[];
    });
  }
  deleteBook(id:any,i:any){
    console.log('checking the id in delete book function',id)
    this.crudService.deleteBook(id).subscribe(data => {
      console.log('checking the data', data)
      this.crudService.getBooks().subscribe(data => {
        console.log('checking the list books in ngonInit',data)
        this.books = data;
      });
    });
  }

}
