import { Component, OnInit, NgZone } from '@angular/core';
// import router angular.router
import { Router } from '@angular/router';
//import my Book service
import { CrudService } from '../../service/crud.service';
// import FormsGroup
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
@Component({
  selector: 'app-add-book',
  templateUrl: './add-book.component.html',
  styleUrls: ['./add-book.component.css']
})
export class AddBookComponent implements OnInit {
  // bookForm = new FormGroup({
  //   id: new FormControl(''),
  //   name: new FormControl('', Validators.required),
  //   price: new FormControl('', Validators.required),
  //   description: new FormControl('', Validators.required)
  // });
  bookForm: FormGroup;

  constructor(
    public formBuilder: FormBuilder,
    private router: Router,
    private ngZone: NgZone,
    private crudService: CrudService,
  ) {
    this.bookForm = this.formBuilder.group({
      id: ['', Validators.required],
      name: ['', Validators.required],
      price: ['', Validators.required],
      description: ['', Validators.required]
    });
  }

  ngOnInit(): void {
  }
  onSubmit(): any {
    this.crudService.addBook(this.bookForm.value).subscribe(data => {
      // this.router.navigate(['/book-list']);
      console.log('checking the data', data)
      this.ngZone.run(() => {
        this.router.navigateByUrl('/books-list');
      },(err:any)=>{
        console.log('error',err)
      }
      )
    });
  }


}
