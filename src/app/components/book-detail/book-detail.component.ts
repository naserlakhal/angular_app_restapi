import { Component, OnInit, NgZone } from '@angular/core';
// import router angular.router
import { Router,ActivatedRoute } from '@angular/router';
//import my Book service
import { CrudService } from '../../service/crud.service';
// import FormsGroup
import { FormGroup, FormBuilder } from '@angular/forms';
@Component({
  selector: 'app-book-detail',
  templateUrl: './book-detail.component.html',
  styleUrls: ['./book-detail.component.css']
})
export class BookDetailComponent implements OnInit {
  // getID from url
 getId:any;
 updateForm : FormGroup;

  constructor(

    public formBuilder: FormBuilder,
    private ActivatedRoute: ActivatedRoute,
    private router: Router,
    private ngZone: NgZone,
    private crudService: CrudService,
  ) { 

    this.getId = this.ActivatedRoute.snapshot.paramMap.get('id');
    this.crudService.getBook(this.getId).subscribe(data => {
      console.log('checking the error data',data['Book'])
      this.updateForm.setValue(data['Book']);
    }
    );
    this.updateForm = this.formBuilder.group({
      id: [''],
      name: [''],
      price: [''],
      description: ['']
    });
  }

  ngOnInit(): void {
  }
  onUpdate(): any {
    this.crudService.updateBook(this.getId,this.updateForm.value).subscribe(data => {
      // this.router.navigate(['/book-list']);
      console.log('checking the data', data)
      this.ngZone.run(() => {
        this.router.navigateByUrl('/books-list');
      },(err:any)=>{
        console.log('error',err)
      }
      )
    });
  }
}
